%define STDIN 0
%define STDOUT 1
%define SYSCALL_READ  0
%define SYSCALL_WRITE 1
%define SYSCALL_EXIT  60
%define NULL_CHAR 0
%define TAB_CHAR 0x9
%define N_CHAR 0xA
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYSCALL_WRITE
    mov rsi, rsp
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 8
    mov rax, rdi
    mov rsi, 0xa
    mov r8, 8
    push 0x0
    .loop:
        xor rdx, rdx
        div rsi
        add rdx, 48
        dec rsp
        mov byte[rsp], dl
        inc rcx
        test rax, rax
        jne .loop
    .align_stack:
        mov rax, rcx
        mov rdi, rsp
        xor rdx, rdx
        div r8
        sub rsp, rdx
        add rcx, rdx
        jmp .end
    .end:
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        ret
; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .length_check:
        push rdi
        push rsi
        call string_length
        mov r8, rax
        pop rsi
        mov rdi, rsi
        push rsi
        call string_length
        pop rsi
        pop rdi
        cmp rax, r8
        jne .not_equals
    xor rax, rax
    .loop:
        test r8, r8
        je .equals
        mov r9b, byte[rsi + rax]
        cmp byte[rdi + rax], r9b
        jne .not_equals
        inc rax
        dec r8
        jmp .loop
    .not_equals:
        xor rax, rax
        ret
    .equals:
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rdx, rdx
    .start_read:
        push rdi
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        cmp rax, ' '
        je .start_read
        cmp rax, TAB_CHAR
        je .start_read
        cmp rax, N_CHAR
        je .start_read
    .read:
        cmp rsi, rdx
        jb .error
        cmp rax, 0x0
        je .end
        cmp rax, ' '
        je .end
        mov byte[rdi + rdx], al
        inc rdx
        push rdi
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        jmp .read
    .end:
        mov [rdi + rdx],  byte 0
        mov rax, rdi
        ret
    .error:
        xor rax, rax
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    mov r9, 0xa
    .loop:
        mov r8b, byte[rdi + rcx]
        xor r8b, '0'
        cmp r8b, 9
        ja .end
        mul r9
        add al, r8b
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r8b, byte[rdi]
    cmp r8b, '-'
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx
    je .end
    inc rdx
    neg rax
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    inc rax
    cmp rdx, rax
    jb .error
    mov r8, rax
    xor rcx, rcx
    .loop:
        mov r9b, byte[rdi + rcx]
        mov byte[rsi + rcx], r9b
        inc rcx
        dec r8
        test r8, r8
        jne .loop
    .end:
        ret
    .error:
        xor rax, rax
        ret
